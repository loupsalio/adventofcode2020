const tools = require('./utils');
let lines = tools.readfile('day05.txt');

let maxSeatId = 0;

lines.map(data => {
  let min = 0
  let max = 127
  for (let index = 0; index <= 6; index++) {
    // console.log(min + ' - ' + max);
    const element = data[index];
    if (index == 6){
      if (element == 'F') {
        max = min;
      }
      else if (element == 'B') {
        min = max;
      }
      break;
    }
    if (element == 'F')
    max = min + (Math.floor((max - min) / 2));
    else if (element == 'B')
    min = min + (Math.ceil((max - min) / 2));
  }
  
  let row = min;

  min = 0
  max = 7
  for (let index = 7; index <= 9; index++) {
    const element = data[index];
    // console.log(element + ' ' + min + ' - ' + max);
    if (index == 9){
      if (element == 'L') {
        max = min;
      }
      else if (element == 'R') {
        min = max;
      }
      break;
    }
    if (element == 'L')
      max = min + (Math.floor((max - min) / 2));
    else if (element == 'R')
      min = min + (Math.ceil((max - min) / 2));
  }

  let seat = min;  
  let seatId = row * 8 + seat;
  
  if (seatId > maxSeatId)
    maxSeatId = seatId;
  // console.log('Row ' + row + ' , seat ' + seat + ' => seatId : ' + seatId);
})

console.log('Max seatID : ' + maxSeatId);