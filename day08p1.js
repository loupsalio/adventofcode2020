const tools = require('./utils');
const _ = require('lodash');
const { isArguments } = require('lodash');

let lines = tools.readfile('day08.txt');

let acc = 0;
let pos = 0;

let hist = [];

function exec(params) {
    let group = params.split(" ");
    let op = group[0];
    let value = parseInt(group[1]);
    if(hist.includes(pos))
        return console.log(acc);
    hist.push(pos);
    switch (op) {
        case "acc":
            acc += value;
            pos += 1;
            break;

        case "jmp":
            pos += value;
            break;

        case "nop":
            pos += 1;
            break;

        default:
            console.log("error : ", params);
            break;
    }

    if(lines[pos])
        exec(lines[pos]);
}

exec(lines[0]);