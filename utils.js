module.exports ={
    readfile : function(filename) {
        var readlines = require('n-readlines');
        var liner = new readlines(filename);

        var edges_number = parseInt(liner.next().toString('ascii'));
        var edges = [];
        var next;
        while (next = liner.next()) {
            edges.push(next.toString('ascii').replace('\r', ''));
        }
        return edges;
    },

    readfileFull : function(filename) {
        var fs = require('fs');
        let data = fs.readFileSync(filename, 'utf8')
        return data;
    },

    nbChar : function(chaine,lettre) {
        var nb = 0;
        chaine = chaine.split(lettre);
        return chaine.length-1;
    }
}