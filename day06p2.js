const tools = require('./utils');

const _ = require('lodash');

let lines = tools.readfileFull('day06.txt');

lines = lines.trim().split('\r\n\r\n')

let max = 0;

lines.map(element => {
  let nb = element.split('\r\n').length;
    arr = element.split('');
    var counts = {};

    for (var i = 0; i < arr.length; i++) {
      var num = arr[i];
      if(num == '\r' || num == '\n')
        continue
      counts[num] = counts[num] ? counts[num] + 1 : 1;
    }
    _.uniq(arr).forEach(e=>{
      if( num == '\r' || num == '\n')
        return
      if (counts[e] == nb)
        max++;
    })
});

console.log(max);