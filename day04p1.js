const tools = require('./utils');
let passports = tools.readfileFull('day04.txt').split('\r\n\r\n');

let list = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

let valids = 0;

passports.map(passport=>{
  passport = passport.split('\r\n').map(x=>x.split(':')[0]);

  // console.log(passport);

  if(list.every(element=>passport.includes(element)))
    valids++;
})

console.log(valids);