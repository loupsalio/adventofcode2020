const tools = require('./utils');
const _ = require('lodash');
const { isArguments } = require('lodash');

let lines = tools.readfile('day07.txt');

let bags = {};

lines.map(line=>{
    let gr = line.split(' contain ');
    let type = gr[0];
    let contains = gr[1].match(/\d( (\w* )*)(bag|bags)/gm);
    console.log(type + (contains ? '' : ' => empty'));
    if(contains)
        contains.map(e=>{
            let name = e.substring(e.indexOf(' ')+1)
            if(!bags[name])
                bags[name] = []
            bags[name].push(type.replace('bags', 'bag'))
            console.log(' - ' + name);
        })
})

let list = [];

function findBag(bag) {
    if (bags[bag]){
        list=list.concat(bags[bag]);
        // console.log(bags[bag]);
        bags[bag].map(x => findBag(x));
    }
}

findBag("shiny gold bag");

console.log(_.uniq(list).length + 1);