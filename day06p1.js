const tools = require('./utils');

const _ = require('lodash');

let lines = tools.readfileFull('day06.txt');

lines = lines.trim().replace('\r\n','\n').split('\r\n\r\n')

let max = 0;

lines.map(element => {
  max += _.uniq(element).filter(e=>e!='\r'&&e!='\n').length;
});

console.log(max);