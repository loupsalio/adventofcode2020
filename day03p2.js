const tools = require('./utils');


function checkTree(x, y){
  lines = tools.readfile('day03.txt').map(x=>x.repeat(100))
  
  let max = lines.length;
  let pos = 0;
  let nbTrees = 0;
  
  for (let index = 0; index < max; index+=y) {
    // console.log(index +'-'+pos + " " + lines[index][pos]);
    if(lines[index][pos] == '#')
      nbTrees++;
    pos += x;
  }
  
  console.log(x + '-' + y + ' => ' + nbTrees);
  return nbTrees;
}

console.log(checkTree(1,1) * checkTree(3,1) * checkTree(5,1) * checkTree(7,1) * checkTree(1,2))