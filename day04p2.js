const tools = require('./utils');
let passports = tools.readfileFull('day04p2.txt').split('\r\n\r\n');

let list = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

let valids = 0;

passports.map(passport => {
  passport = passport.split('\n').map(x => x.split(':'));
  // console.log(passport);
  let passport_list = passport.map(x => x[0])
  // console.log(passport);
  if (list.every(element => passport_list.includes(element))) {
    let v = 0;
    console.log(passport);
    passport.map(el => { // Comp
      switch (el[0]) {
        case 'byr':
          // console.log(parseInt(el[1]));
          if (!el[1].replace('\r', '').match(/[0-9]{4}/gm) || el[1].replace('\r', '').length != 4 || parseInt(el[1]) < 1920 || parseInt(el[1]) > 2002)
            {v = 1;console.log(el[0] + ' (1920 - 2002) x ' + el[1]);}else{console.log(el[0] + ' -> ' + el[1]);}
          break;
        case 'iyr':
          if (!el[1].replace('\r', '').match(/[0-9]{4}/gm) || el[1].replace('\r', '').length != 4 || parseInt(el[1]) < 2010 || parseInt(el[1]) > 2020)
            {v = 1;console.log(el[0] + ' (2010 - 2020) x ' + el[1]);}else{console.log(el[0] + ' -> ' + el[1]);}
          break;
        case 'eyr':
          if (!el[1].replace('\r', '').match(/[0-9]{4}/gm) || el[1].replace('\r', '').length != 4 || parseInt(el[1]) < 2020 || parseInt(el[1]) > 2030)
            {v = 1;console.log(el[0] + ' (2020 - 2030) x ' + el[1]);}else{console.log(el[0] + ' -> ' + el[1]);}
          break;
        case 'hgt':
          if (!el[1].replace('\r', '').match(/(1[5-8][0-9]|19[0-3])cm|(59|6[0-9]|7[0-6])in/gm))
            {v = 1;console.log(el[0] + ' x ' + el[1]);}else{console.log(el[0] + ' -> ' + el[1]);}
          break;
        case 'hcl':
          if (el[1].replace('\r', '').length != 7 || !el[1].replace('\r', '').match(/#[0-9a-f]{6}/gm))
            {v = 1;console.log(el[0] + ' x ' + el[1]);}else{console.log(el[0] + ' -> ' + el[1]);}
          break;
        case 'ecl':
          if (el[1].replace('\r', '').length != 3 || !el[1].replace('\r', '').match(/amb|blu|brn|gry|grn|hzl|oth/gm))
            {v = 1;console.log(el[0] + ' x ' + el[1]);}else{console.log(el[0] + ' -> ' + el[1]);}
          break;
        case 'pid':
          if (el[1].replace('\r', '').length != 9 || !el[1].replace('\r', '').match(/[0-9]{9}/gm))
            {v = 1;console.log(el[0] + ' x ' + el[1]);}else{console.log(el[0] + ' -> ' + el[1]);}
          break;
      }
    })
    console.log('passport status ' + v);
    if (v == 0)
      valids++
  }
})

console.log(valids);