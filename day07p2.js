const tools = require('./utils');
const _ = require('lodash');
const { isArguments } = require('lodash');

let lines = tools.readfile('day07.txt');

let bags = {};

lines.map(line=>{
    let gr = line.split(' contain ');
    let type = gr[0].replace('bags', 'bag');
    let contains = gr[1].match(/\d( (\w* )*)(bag|bags)/gm);
    // console.log(type + (contains ? '' : ' => empty'));
    if(contains)
        contains.map(e=>{
            let name = e.substring(e.indexOf(' ')+1).replace('bags', 'bag')
            let nb = parseInt(e)
            if(!bags[type])
                bags[type] = {}
            bags[type][name] = nb
            // console.log(type + ' - ', bags[type]);
        })
})

function findBag(target){
    let nb = 0;
    let bag = bags[target];

    console.log(target);
    if (!bag){
        console.log(1);
        return 1;
    }
    Object.keys(bag).forEach((key) => {
        var val = bag[key];
        console.log(`- ${key}: `, val, nb);
        nb += val * findBag(key)
    });

    return nb;
}

// let list = [];

// function findBag(bag) {
//     if (bags[bag]){
//         list=list.concat(bags[bag]);
//          console.log(bags[bag]);
//         bags[bag].map(x => findBag(x));
//     }
// }

console.log(findBag("shiny gold bag"));

// console.log(_.uniq(list).length + 1);