const utils = require('./utils');
const tools = require('./utils');

lines = tools.readfile('day02.txt').map(x=>x.trim().split(':'))

let valids = 0;

lines.map(line=>{
  let passwd = line[1].trim();

  let tmp = line[0].split(' ');
  let char = tmp[1];
  tmp = tmp[0].split('-');
  let min = parseInt(tmp[0]);
  let max = parseInt(tmp[1]);

  // console.log(min + '-' + max + ' ' + char + ": " + passwd);

  tmp = tools.nbChar(passwd,char)
  if (tmp >= min && tmp <= max)
    valids++;
})

console.log(valids);