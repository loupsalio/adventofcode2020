const tools = require('./utils');

lines = tools.readfile('day03.txt').map(x=>x.repeat(32))

let max = lines.length;
let pos = 0;
let nbTrees = 0;

for (let index = 0; index < max; index++) {
  // console.log(index +'-'+pos + " " + lines[index][pos]);
  if(lines[index][pos] == '#')
    nbTrees++;
  pos += 3;
}

console.log(nbTrees);