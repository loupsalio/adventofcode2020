const utils = require('./utils');
const tools = require('./utils');

lines = tools.readfile('day02.txt').map(x=>x.trim().split(':'))

let valids = 0;

lines.map(line=>{
  let passwd = line[1].trim();

  let tmp = line[0].split(' ');
  let char = tmp[1];
  tmp = tmp[0].split('-');
  let pos1 = parseInt(tmp[0])-1;
  let pos2 = parseInt(tmp[1])-1;

  // console.log(min + '-' + max + ' ' + char + ": " + passwd);
  // console.log(passwd[pos1] + '-' + passwd[pos2] + ' ' + char + ": " + passwd);

  if ((passwd[pos1] == char && passwd[pos2] != char) || (passwd[pos1] != char && passwd[pos2] == char))
    valids++;
})

console.log(valids);